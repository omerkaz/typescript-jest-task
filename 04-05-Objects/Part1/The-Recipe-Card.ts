interface IfavRecipe {
    title: string 
    servings : number
    ingredients: string[]
}

const favRecipe: IfavRecipe = {
    title: "Yalancı Tavuk Göğsü",
    servings : 1,
    ingredients : [
        "tereyağı",
        "un",
        "şeker",
        "süt",
        "vanilya"
    ]
}

const logTheRecipe = (object: IfavRecipe) => {
    let key: keyof IfavRecipe
    for(key in object) {
        if(Array.isArray(object[key])) {
          let ingredientsArr = object[key] as string[]
          ingredientsArr.map((item: string) => console.log(item))
        } else {
            console.log(object[key])
        }
    }
}