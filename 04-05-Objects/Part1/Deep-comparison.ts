const deepEqual = (object1: { [x: string]: any}, object2: { [x: string]: any}): boolean => {
    for(const key in object1) {
        if(object1.hasOwnProperty(key) !== object2.hasOwnProperty(key)) return false
        if(typeof object1[key] !== typeof object2[key]) return false
        if(typeof object1[key] !== "object") {
            if(object1[key] !== object2[key]) return false
        }
        if(!deepEqual(object1[key], object2[key])) return false
    }
    return true
}