// 1.5	The sum of a range
// The introduction of this book alluded to the following as a nice way to compute the sum of a range of numbers:
// console.log(sum(range(1, 10)));
// Write a range function that takes two arguments, start and end, and returns an array containing all the numbers from start up to (and including) end.
// Next, write a sum function that takes an array of numbers and returns the sum of these numbers. Run the previous program and see whether it does indeed return 55.

const range = (start: number, end: number): number[] => {
  let numArr = [];
  for (let i = start; i <= end; i++) {
    numArr.push(i);
  }
  return numArr;
};

const sum = (array: number[]) => {
  let sumOfArr = array.reduce(
    (preValue, currentValue) => preValue + currentValue, 0
  );
  return sumOfArr
};
