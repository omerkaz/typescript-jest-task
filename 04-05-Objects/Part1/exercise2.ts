import { student } from "./exercise1";

// 1.2	Exercise 2

// Write a JavaScript program to delete the rollno property from the following object. Also print the object before or after deleting the property 
// Sample object : 
// var student = { 
// name : "David Rayy", 
// sclass : "VI", 
// rollno : 12 };

const exercise2 = () => {
    console.log(student)
    const {rollno, ...newStudent} = student
    console.log(newStudent)
}