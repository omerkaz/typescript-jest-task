// 1.1	Exercise  1
// Write a JavaScript program to list the properties of a JavaScript object 
// Sample object : 
// var student = { 
// name : "David Rayy", 
// sclass : "VI", 
// rollno : 12 };
// Sample Output : name,sclass,rollno



interface IStudent {
    name: string
    sclass: string
    rollno: number
}

export const student: IStudent = {
    name: "Fikret",
    sclass: "VII",
    rollno: 7
}

const exercise1 = () => {
   console.log(Object.keys(student)) 
}