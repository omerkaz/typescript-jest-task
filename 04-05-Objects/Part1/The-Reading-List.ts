interface ImyBooks {
  title: string;
  author: string;
  alreadyRead: boolean;
}

const myBooks: ImyBooks[] = [
  {
    title: "Yanılgının İcadı",
    author: "Reha Nazlı",
    alreadyRead: false,
  },
  {
    title: "Mutlu Beyin",
    author: "Loretta Breuning",
    alreadyRead: false,
  },
  {
    title: "Of Not Being A Jew",
    author: "İsmet Özel",
    alreadyRead: true,
  },
];

const readingListOutput = (arrObject: ImyBooks[]) => {
  arrObject.map((item) =>
    item.alreadyRead
      ? console.log(`You already read "${item.title}" by ${item.author}`)
      : console.log(`'You still need to read "${item.title} by ${item.author}"`)
  );
};
