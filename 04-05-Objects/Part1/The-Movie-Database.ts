interface IfavMovie {
    title : string
    duration : number
    stars : string[]
    [key: string] : any
}

const favMovie: IfavMovie = {
    title : "Constantine",
    duration : 2,
    stars : ["Keanu Reeves", "Rachel Weisz"]
}

const favMovieOutput = (object: IfavMovie) => {
    console.log(`${object.title} lasts for ${object.duration} hours. Stars: ${object.stars.map((item) => item)}`)
}