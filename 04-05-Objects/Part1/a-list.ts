const arrayToList = (arr: any[]) => {
    let list = null
    for(let i = arr.length -1; i >= 0; i--) {
        list = {
            value: arr[i],
            rest: list
        }
    }
    return list
}

const listToArray = (list: { [x: string]: any}): number[] => {
    let arr = []
    for(const key in list) {
        typeof list[key] === "object" ? arr.push(listToArray(list[key])) : arr.push(list[key])
    }
    arr = arr.concat(...arr)
    return arr
}

const prepend = (value: number, rest: object): object => {
    let list = {
        value : value,
        rest : rest
    }
    return list
}

const nth = (list: {value : number, rest: {value: number; rest: any } }, number: number): number => {
    return number === 0 ? list.value : nth(list.rest, number - 1)
}

