// 1.3	Exercise 3
// Write a JavaScript program to display the reading status (i.e. display book name, author name and reading status) of the following books. var library = [ 
//    {
//        author: 'Bill Gates',
//        title: 'The Road Ahead',
//        readingStatus: true
//    },
//    {
//        author: 'Steve Jobs',
//        title: 'Walter Isaacson',
//        readingStatus: true
//    },
//    {
//        author: 'Suzanne Collins',
//        title:  'Mockingjay: The Final Book of The Hunger Games', 
//        readingStatus: false
//    }];

type libraryType = {
  author: string;
  title: string;
  readingStatus: boolean;
};
export let library: libraryType[] = [
  {
    author: "Bill Gates",
    title: "The Road Ahead",
    readingStatus: true,
  },
  {
    author: "Steve Jobs",
    title: "Walter Isaacson",
    readingStatus: true,
  },
  {
    author: "Suzanne Collins",
    title: "Mockingjay: The Final Book of The Hunger Games",
    readingStatus: false,
  },
];

const showObject = () => {
    library.map((item) => {
        console.log(`Author name ${item.author}, Book name ${item.title}, Reading status ${item.readingStatus}`)
    })
};
