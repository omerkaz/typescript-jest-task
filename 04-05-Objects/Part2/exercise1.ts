function Cylinder(this: { radius: number, height: number }, radius: number, height: number ) {
    this.radius = radius
    this.height = height
}

Cylinder.prototype.getVolume = function() {
    return Math.PI * Math.pow(this.radius, 2) * this.height
}

let cyl = new (Cylinder as any)(5,5)

console.log(cyl.getVolume().toFixed(4))