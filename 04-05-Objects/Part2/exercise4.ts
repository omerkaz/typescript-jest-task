function Clock(this: { timeRenewal: number }, timeRenewal: number) {
  this.timeRenewal = timeRenewal;
}

Clock.prototype.getClock = function () {
  setInterval(() => {
    let date = new Date();
    let hours = date.getHours();
    let min = date.getMinutes();
    let sec = date.getSeconds();
    console.log(`${hours}:${min}:${sec}`);
  }, this.timeRenewal);
};

let testClock = new (Clock as any)(1000);

testClock.getClock();
