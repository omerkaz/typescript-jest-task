class Vector {
  x: number;
  y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  plus(vektor: { x: number; y: number }) {
    return new (Vector as any)(this.x + vektor.x, this.y + vektor.y);
  }

  minus(vektor: { x: number; y: number }) {
    return new (Vector as any)(this.x - vektor.x, this.y - vektor.y);
  }

  length() {
    return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
  }
}

console.log(new (Vector as any)(3, 5).plus(new (Vector as any)(3, 5)));
