function Circle(this: { radius: number }, radius: number) {
  this.radius = radius;
}

Circle.prototype.area = function () {
  return Math.PI * Math.pow(this.radius, 2);
};

Circle.prototype.perimeter = function () {
  return Math.PI * this.radius * 2;
};

let testCircle = new (Circle as any)(1);

console.log(testCircle.perimeter(), testCircle.area());
