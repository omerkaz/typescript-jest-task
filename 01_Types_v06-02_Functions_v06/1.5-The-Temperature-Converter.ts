// 1.5	The Temperature Converter
// It's hot out! Let's make a converter based on the steps here.
// •	Store a celsius temperature into a variable.
// •	Convert it to fahrenheit and output "NN°C is NN°F".
// •	Now store a fahrenheit temperature into a variable.
// •	Convert it to celsius and output "NN°F is NN°C."
// fonksiyon ismini duzelt
const temperatureConvert = (celsius: number, fahrenheit: number) => {
  // burdaki degerleri constant olarak tutabilirsin, ornegin burdaki 32 nin ne oldugunu bilmiyorum ben
  // dogru isimlendirmeyle tutman daha dogru,
  //const CELCIUS_TO_FAHRENHEIT_CONSTANT = 32 gibi
  const converterValues = {
    celsiusToFahrenheitMultiply: 9 / 5,
    fahrenheitToCelsiusMultiply: 5 / 9,
    summationOrSubtractValue: 32,
  };

  const celsiusToFahrenheit = (
    celsius * converterValues.celsiusToFahrenheitMultiply +
    converterValues.summationOrSubtractValue
  ).toPrecision(2);

  const fahrenheitToCelsius = (
    (fahrenheit - converterValues.summationOrSubtractValue) *
    converterValues.fahrenheitToCelsiusMultiply
  ).toPrecision(2);

  return `${celsius}°C is ${celsiusToFahrenheit}°F.
    ${fahrenheit}°F is ${fahrenheitToCelsius}°C.
    `;
};

export default temperatureConvert;
