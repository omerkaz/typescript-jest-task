// 1.4	The Geometrizer
// Calculate properties of a circle, using the definitions here.
// PI kann man durch Math.PI verwenden
// •	Store a radius into a variable.
// •	Calculate the circumference based on the radius, and output "The circumference is NN".
// •	Calculate the area based on the radius, and output "The area is NN".
// fonksiyon isimleri bir fiil belirtmeli,isim fonksiyon ismi uygun degil
const  circleCalculate = (radius: number) => {
  //const
  const circumference = (2 * Math.PI * radius).toPrecision(2);
  const area = (Math.PI * Math.pow(radius, 2)).toPrecision(2);
  return `
    The circumference is ${circumference}
    The area is ${area}
    `;
};

export default circleCalculate;
