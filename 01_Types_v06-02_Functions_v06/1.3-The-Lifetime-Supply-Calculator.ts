// 1.3	The Lifetime Supply Calculator
// Ever wonder how much a "lifetime supply" of your favorite snack is? Wonder no more!
// •	Store your current age into a variable.
// •	Store a maximum age into a variable.
// •	Store an estimated amount per day (as a number).
// •	Calculate how many you would eat total for the rest of your life.
// •	Output the result to the screen like so: "You will need NN to last you until the ripe old age of X".
    // kisaltma kullanma
const lifeTimeSupplyCalculate = (currentAge: number, maximumAge: number, perDaySnack: number) => {
    // 	Store an estimated amount per day (as a number).\
    //const
    const theRestOfLife = ((maximumAge - currentAge) * 365) * perDaySnack
    return `You will need ${theRestOfLife} to last you until the ripe old age of ${maximumAge}`
}
//export duzelt
export default lifeTimeSupplyCalculate
