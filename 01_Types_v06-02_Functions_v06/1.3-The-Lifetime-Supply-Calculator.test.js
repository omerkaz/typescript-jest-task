const {lifeTimeSupplyCalculate} = require("./1.3-The-Lifetime-Supply-Calculator")

test("The Lifetime Supply Calculator", () => {
    expect(lifeTimeSupplyCalculate(25, 60, 3)).toBe("You will need 38325 to last you until the ripe old age of 60")
})