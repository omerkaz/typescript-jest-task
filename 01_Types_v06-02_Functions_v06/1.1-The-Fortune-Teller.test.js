const { theFortuneTeller } = require('./1.1-The-Fortune-Teller')

test('The Fortune Teller', () => {
  expect(theFortuneTeller(2,"Prenses Ela","Çorum","Tesisatçı")).toBe("You will be a Tesisatçı in Çorum, and married to Prenses Ela with 2 kids.");
})