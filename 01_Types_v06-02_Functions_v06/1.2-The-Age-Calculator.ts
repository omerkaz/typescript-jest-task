// 1.2	The Age Calculator
// Forgot how old someone is? Calculate it!
// •	Store the current year in a variable.
// •	Store the birth year in a variable.
// •	Calculate the 2 possible ages based on the stored values.
// •	Output them to the screen like so: "You are either NN or NN", substituting the values.

const calculateAge = (currentYear: number, birthYear: number) => {
    // let degil const
    // ya dogum yili current yeardan buyukse?
    
    const ageResult = currentYear - birthYear
    return ageResult < 0 ? `You can't come from the future :)` : `You are either ${ageResult} or ${ageResult-1}.`
}

// exportu duzelt
export default calculateAge
