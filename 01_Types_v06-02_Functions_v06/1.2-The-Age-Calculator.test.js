const {calculateAge} = require("./1.2-The-Age-Calculator")

test("The Age Calculator", () => {
    expect(calculateAge(2022, 1997)).toMatch("You are either 25 or 24.")
})