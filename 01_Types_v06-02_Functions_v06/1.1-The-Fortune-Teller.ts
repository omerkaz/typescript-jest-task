// 1.1	The Fortune Teller
// Why pay a fortune teller when you can just program your fortune yourself?
// •	Store the following into variables: number of children, partner's name, geographic location, job title.
// •	Output your fortune to the screen like so: "You will be a X in Y, and married to Z with N kids."

const theFortuneTeller = (numOfChilds: number, partnerName: string, location: string, jobTitle: string) => {
    // Burada dogrudan return edebilirsin, eger variableye ihtiyacin varsa da let degil const kullanmaya calis
    return `You will be a ${jobTitle} in ${location}, and married to ${partnerName} with ${numOfChilds} kids.`
}
// typescriptde dogrudan export edebilmen lazim, module exportsu degistir
export default theFortuneTeller
