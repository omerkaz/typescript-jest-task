import { pluralize } from "./The-Pluralizer";

describe("Simple number and string check", () => {
    test("Noun and number check", () => {
        expect(pluralize(2, "dog")).toBe("2 dogs")
    })
})