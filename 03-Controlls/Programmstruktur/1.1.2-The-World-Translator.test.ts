import { helloWorld } from "./1.1.2-The-World-Translator";

describe("Simple string check", () => {
    test("Languages check", () => {
        expect(helloWorld("es")).toBe("Hola, Mundo")
    })
})