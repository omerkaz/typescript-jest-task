// 1.1.5	The even/odd reporter
// Write a for loop that will iterate from 0 to 20. For each iteration, 
// it will check if the current number is even or odd, and report that to the screen (e.g. "2 is even").

const evenOddReporter = () => {
    const reportArr = []
    for(let i = 0; i <= 20; i++) {
        i % 2 === 0 ? reportArr.push(`${i} is even`) : reportArr.push(`${i} is odd`)
    }
    return reportArr
}

export {evenOddReporter}