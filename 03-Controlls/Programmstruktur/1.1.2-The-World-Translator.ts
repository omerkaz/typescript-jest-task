// 1.1.2	The World Translator
// •	Write a function named helloWorld that:
// o	takes 1 argument, a language code (e.g. "es", "de", "en")
// o	returns "Hello, World" for the given language, for at least 3 languages. It should default to returning English.
// •	Call that function for each of the supported languages and log the result to make sure it works.

const helloWorld = (languageCode: string) => {
    switch (languageCode) {
        case "de":
            return "Hallo, Welt"
            break;
        case "es":
            return "Hola, Mundo"
            break;
        default:
            "Hello, World"
    }
}

export {helloWorld}