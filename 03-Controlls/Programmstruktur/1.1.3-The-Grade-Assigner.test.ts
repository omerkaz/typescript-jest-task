import { assignGrade } from "./1.1.3-The-Grade-Assigner";

describe("Simple number check", () => {
    test("Grade check", () => {
        expect(assignGrade(80)).toBe("C")
    })
})