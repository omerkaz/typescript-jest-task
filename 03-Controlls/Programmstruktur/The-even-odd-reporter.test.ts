import { evenOddReporter } from "./The-even-odd-reporter";

describe("Simple check for loop", () => {
    test("Check loop array", () => {
        expect(evenOddReporter()).toHaveLength(21)
    })
})