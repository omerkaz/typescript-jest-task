// 1.1.4	The Pluralizer
// •	Write a function named pluralize that:
// o	takes 2 arguments, a noun and a number.
// o	returns the number and pluralized form, like "5 cats" or "1 dog".
// •	Call that function for a few different scores and log the result to make sure it works.
// •	Bonus: Make it handle a few collective nouns like "sheep" and "geese".

const pluralize = (number: number, noun: string) => {
    if(number > 1 && noun !== "sheep" && noun !== "geese") return `${number} ${noun}s`
    else return `${number} ${noun}`
}

export {pluralize}