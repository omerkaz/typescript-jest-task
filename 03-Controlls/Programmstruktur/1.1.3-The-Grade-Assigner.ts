// 1.1.3	The Grade Assigner
// •	Write a function named assignGrade that:
// o	takes 1 argument, a number score.
// o	returns a grade for the score, either "A", "B", "C", "D", or "F".
// •	Call that function for a few different scores and log the result to make sure it works.
// •	Hinweis: Score von 0-100, A  > 90 <= 100, B > 80 <= 90, C > 70 <= 80,  D > 65 <= 70, F <= 65

const assignGrade = (grade: number) => {
    if (grade > 65) {
        switch (true) {
            case grade < 71:
                return "D"
                break;
            case grade < 81:
                return "C"
                break;
            case grade < 91:
                return "B"
                break;
            case grade < 101:
                return "A"
                break;
        }
    } else {
        return "F"
    }
}

export { assignGrade }
