import greaterNumber from "./1.1.1-What-number's-bigger"

describe("Simple number check", () => {
    test("Check numbers with greaterNum func", () => {
        expect(greaterNumber(1,2)).toEqual(2)
    })
})