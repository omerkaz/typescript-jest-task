// 1.1.1	What number's bigger?
// •	Write a function named greaterNum that:
//      o	takes 2 arguments, both numbers.
//      o	returns whichever number is the greater (higher) number.
// •	Call that function 2 times with different number pairs, and log the output to make sure it works (e.g. "The greater number of 5 and 10 is 10.").
//fonksiyon isimleri, parametreleri kisaltma
// taskta bu sekilde istediği icin yazmistim, düzelttim
const greaterNumber = (number_1: number, number_2: number) => {
    //const kullan
    const  greaterNumber = number_1 > number_2 ? number_1 : number_2
    console.log(`The greater number of ${number_1} and ${number_2} is ${greaterNumber}.`)
    return greaterNumber
}
// exportu yukari yazabilirsin
export default greaterNumber
