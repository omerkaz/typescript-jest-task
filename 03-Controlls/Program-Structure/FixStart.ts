const fixStart = (str: string) => {
    const firstLetter = str.slice(0, 1);
    const strArr = str.split("");
    strArr.forEach((letter, index) => {
        if (index !== 0 && letter === firstLetter) strArr[index] = "*";
    });
    return `${strArr.join("")}`;
}
