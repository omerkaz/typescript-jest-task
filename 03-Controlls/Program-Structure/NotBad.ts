const notBad = (str: string) => {
    if (
        str[str.length - 1] === "." ||
        str[str.length - 1] === "?" ||
        str[str.length - 1] === "!"
    )
        str = str.substring(0, str.length - 1);

    let strArr = str.split(" ");

    if (!strArr.includes("bad") || !strArr.includes("not")) return `${str}`;

    if (strArr.indexOf("not") < strArr.indexOf("bad")) {
        strArr = strArr.filter((item) => {
            return strArr.indexOf("not") <= strArr.indexOf(item) ? false : true;
        });
        strArr.push("good");
        return strArr.join(" ");
    }
}