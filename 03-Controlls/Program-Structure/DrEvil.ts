
const drEvil = (amount: number) => {
    return amount > 1000000 ? `${amount} dollars (pinky)` : `${amount} dollars`
}