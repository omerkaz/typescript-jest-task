const mixUp = (str1: string, str2: string) => {
    const holdingStr1 = str1;
    const str1Arr = str1.split("");
    const str2Arr = str2.split("");
    for (let i = 0; i < 2; i++) {
        str1Arr[i] = str2Arr[i];
        str2Arr[i] = holdingStr1[i];
    }
    return `${str1Arr.join("")} ${str2Arr.join("")}`;
}