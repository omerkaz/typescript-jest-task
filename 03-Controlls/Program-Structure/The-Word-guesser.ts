
const readline = require("readline")

const rl = readline.createInterface(
  process.stdin, process.stdout);


const wordLettersArr = ["F", "O", "X"]
let guessedLettersArr = ["_", "_", "_"]

const guessLetter = (letter: string) => {
  if (wordLettersArr.includes(letter.toUpperCase())) guessedLettersArr[wordLettersArr.indexOf(letter.toUpperCase())] = letter.toUpperCase()
  if (guessedLettersArr.indexOf("_") === -1) rl.close()
  console.log(guessedLettersArr)
  if (guessedLettersArr.indexOf("_") !== -1) rl.question("Bir harf giriniz: ", function (letter: string) { guessLetter(letter) })
}

console.log("Harf tahmin oyununa hoşgeldiniz.")
rl.question("Bir harf giriniz: ", function (letter: string) {
  if (guessedLettersArr.indexOf("_") !== -1) guessLetter(letter)
})








