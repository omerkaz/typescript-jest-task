
const squareNumber = (num: number) => {
    console.log(`The result of squaring the number ${num} is ${Math.pow(num, num)}.`)
    return Math.pow(num, num)

}

const halfNumber = (num: number) => {
    console.log(`Half of ${num} is ${num / 2}.`)
    return num / 2
}

const percentOf = (percentNum: number, num: number) => {
    console.log(`${num} is ${percentNum}% of ${(num / 100) * percentNum}."`)
    return (num / 100) * percentNum
}

const areaOfCircle = (radius: number) => {
    console.log(`The area for a circle with radius ${radius} is ${radius * radius * Math.PI}.`)
    return radius * radius * Math.PI
}

export { squareNumber, halfNumber, percentOf, areaOfCircle }