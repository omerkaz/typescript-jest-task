const verbing = (verb: string) => {
    return verb.length < 3 ? `${verb}` : verb.slice(verb.length - 3, verb.length) === "ing" ? `${verb}ly` : `${verb}ing`
}